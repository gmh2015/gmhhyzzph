
## 2019 全国研究生数学建模竞赛 LaTeX 论文

# 编译
采用xelatex编译  
建议编译顺序为
```bash
xelatex paper.tex
bibtex paper
xelatex paper.tex
xelatex paper.tex
```
编写了make.bat文件供windows下批处理编译

# 文件
code文件夹中为实现的代码  
figure文件夹中为论文中所使用的图片