import numpy as np
from scipy.optimize import fsolve

# Format
# points=[[],[],[],[]]
# truth=np.array([dx1,dy1,dx2,dy2,...])
def solve(init_guess,points,truth,d,h):
    def f(x):
        [xo,yo,zo,thetax,thetay,nx,ny,nz]=[i for i in x]
        ans=[]
        for point in points:
            oa=np.array([point[0] - xo,point[1]- yo,-zo])
            p = np.array([-nx*nz,ny*nz,-(nx*nx+ny*ny)])
            l = np.array([ny,-nx,0])
            n = np.array(nx,ny,nz)
            oa_cross_p=np.cross(oa,p)
            oa_cross_l=np.cross(oa,l)
            normpl = np.linalg.norm(oa_cross_p) * np.linalg.norm(l)
            normlp = np.linalg.norm(oa_cross_l) * np.linalg.norm(p)
            phix = np.arccos(np.dot(oa_cross_p,l)/normpl)
            phiy = np.arccos(np.dot(oa_cross_l,p)/normlp)
            ans.append(d * np.tan(phix)/(2 * np.tan(thetax / 2)))
            ans.append(h * np.tan(phiy)/(2 * np.tan(thetay / 2)))
        return np.array(ans - truth)
    return fsolve(f,init_guess)

init_guess=np.array([])
points=[[],[],[],[]]
truth=np.array([])
d=1920
h=1080
print(solve(init_guess,points,truth,d,h))