import numpy as np
import cv2
from matplotlib import pyplot as plt
from PIL import Image

imgL = cv2.imread('img_left.jpg',cv2.IMREAD_GRAYSCALE)
imgR = cv2.imread('img_right.jpg',cv2.IMREAD_GRAYSCALE)


stereo = cv2.StereoBM_create(numDisparities=16, blockSize=5)
disparity = stereo.compute(imgL,imgR)

cv2.imwrite('disparity-map.jpg',disparity)