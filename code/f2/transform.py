import cv2 
import numpy as np 
img = cv2.imread('f2.png')
#img = cv2.resize(img,(0,0),fx=0.3,fy=0.3)
rows,cols,_=img.shape 
print(img.shape)
point_size = 1 
point_color = (0, 0, 255) 
thickness = 5
points1 = np.float32([[770,979],[883,1029],[1306,1023],[1197,995]])
points2 = np.float32([[1500,1350],[1600,1350],[1600,1250],[1500,1250]])
matrix = cv2.getPerspectiveTransform(points1,points2) 
output = cv2.warpPerspective(img,matrix,(cols,rows))
for point in points1: 
	cv2.circle(img, (point[0],point[1]), point_size, point_color, thickness)
for point in points2: 
	cv2.circle(output, (point[0],point[1]), point_size, point_color, thickness)

def compute(x,y):
	new_x=matrix[0,0]*x+matrix[0,1]*y+matrix[0,2]
	new_y=matrix[1,0]*x+matrix[1,1]*y+matrix[1,2]
	new_z=matrix[2,0]*x+matrix[2,1]*y+matrix[2,2]
	new_x = int(new_x / new_z)
	new_y = int(new_y / new_z)
	cv2.circle(img, (x,y), point_size, (0,0,255), 25)
	cv2.circle(output, (new_x,new_y), point_size, (0,0,255),25)
	return (new_x,new_y)


print (compute(1414,1053))#A车
print (compute(644,875)) #C车
print (compute(882,927)) #B
# print (compute(664,745))#标线1
# print (compute(708,749))#标线2
# print (compute(836,724))#标线3

# cv2.imshow('img',img)
# cv2.imshow('output',output)
cv2.imwrite('pers-transform-before.jpg',img) 
cv2.imwrite('pers-transform-after.jpg',output) 
# cv2.waitKey()
# cv2.destroyAllWindows()
import os
os.system('pers-transform-after.jpg')