import cv2
import numpy as np
img = cv2.imread('test.jpg')
img = cv2.resize(img,(0,0),fx=0.3,fy=0.3)
rows,cols,_=img.shape
print(img.shape)

point_size = 1
point_color = (0, 0, 255) # BGR
thickness = 4 # 可以为 0 、4、8

points1 = np.float32([[20,65],[310,52],[28,200],[400,210]])
points2 = np.float32([[100,100],[400,100],[100,200],[400,200]])

matrix = cv2.getPerspectiveTransform(points1,points2)
print(matrix)
output = cv2.warpPerspective(img,matrix,(cols,rows))

for point in points1:
	cv2.circle(img, (point[0],point[1]), point_size, point_color, thickness)

for point in points2:
	cv2.circle(output, (point[0],point[1]), point_size, point_color, thickness)

cv2.imshow('img',img)
cv2.imshow('output',output)
cv2.imwrite('pers-transform-before.jpg',img)
cv2.imwrite('pers-transform-after.jpg',output)
cv2.waitKey()
cv2.destroyAllWindows()
