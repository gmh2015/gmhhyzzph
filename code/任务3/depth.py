import numpy as np
import cv2
from matplotlib import pyplot as plt
from PIL import Image
imgL = cv2.imread('L.jpg',cv2.IMREAD_GRAYSCALE)
imgR = cv2.imread('R.jpg',cv2.IMREAD_GRAYSCALE)
#plt.imshow(imgL,'fff')
#plt.show()
stereo = cv2.StereoBM_create(numDisparities=16, blockSize=5)
disparity = stereo.compute(imgL,imgR)
disparity = cv2.medianBlur(disparity, 9)
print(disparity.shape)
distance = np.zeros([1090,1920])
for i in range(disparity.shape[0]):
    for j in range(disparity.shape[1]):
        if disparity[i,j] <= 0:
            continue
        else:
            distance[i,j] = 1 / disparity[i,j]
            if distance[i,j] > 0.04:
                distance[i,j] = 0
            
        
#disparity=Image(disparity)
#width,height=disparity.size
#print (disparity)
# for i in range(0,1080):
#     for j in range(0,1920):
#         if disparity[i][j]>0:
#            disparity[i][j]=0.026*2.78/disparity[i][j]
#         else:
#             disparity[i][j]=255
#width=disparity.size[0]
plt.figure('disparity')
plt.imshow(disparity)
plt.figure('distance')
gci = plt.imshow(distance)
cbar = plt.colorbar(gci)
plt.show()
