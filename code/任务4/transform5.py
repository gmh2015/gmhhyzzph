import cv2 
import numpy as np 
img = cv2.imread('frame_727.jpg')
#img = cv2.resize(img,(0,0),fx=0.3,fy=0.3)
rows,cols,_=img.shape 
print(img.shape)
point_size = 1 
point_color = (0, 0, 255) 
thickness = 5
points1 = np.float32([[521,411],[518,480],[621,549],[630,473]])
points2 = np.float32([[100,400],[100,600],[400,600],[400,400]])
matrix = cv2.getPerspectiveTransform(points1,points2) 
output = cv2.warpPerspective(img,matrix,(cols,rows))
for point in points1: 
	cv2.circle(img, (point[0],point[1]), point_size, point_color, thickness)
for point in points2: 
	cv2.circle(output, (point[0],point[1]), point_size, point_color, thickness)

def compute(x,y):
	new_x=matrix[0,0]*x+matrix[0,1]*y+matrix[0,2]
	new_y=matrix[1,0]*x+matrix[1,1]*y+matrix[1,2]
	new_z=matrix[2,0]*x+matrix[2,1]*y+matrix[2,2]
	new_x = int(new_x / new_z)
	new_y = int(new_y / new_z)
	cv2.circle(img, (x,y), point_size, (0,0,255), 10)
	cv2.circle(output, (new_x,new_y), point_size, (0,0,255),10)
	return (new_x,new_y)

print (compute(955,453))#右门
#print (compute(854,454)) #左门
# print (compute(59,495)) #C
# print (compute(355,283)) #D
# print (compute(864,1131)) #人头顶
# print (compute(882,1358)) #人脚底
# print (compute(508,1409)) #左延长线
# print (compute(968,1394)) #右延长线



# cv2.imshow('img',img)
# cv2.imshow('output',output)
cv2.imwrite('pers-transform-before.jpg',img) 
cv2.imwrite('pers-transform-after.jpg',output) 
# cv2.waitKey()
# cv2.destroyAllWindows()